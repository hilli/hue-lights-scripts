#!/usr/bin/env ruby

require 'huey'

Huey.configure do |config|
  # For discovering the Hue hub, usually you won't have to change this
  config.ssdp_ip = '239.255.255.250' 

  # Also for discovering the Hue hub
  config.ssdp_port = 1900            

  # If you get constant errors about not being able to find the Hue hub and you're sure it's connected, increase this
  config.ssdp_ttl = 1

  # Change this if you don't like the included uuid
  config.uuid = '001788fffe0a5dfc'
end


#bulbs = Huey::Bulb.all # Returns an array of your bulbs
bulbs = []
bulbs << Huey::Bulb.find(2)
bulbs << Huey::Bulb.find(3)
#pp bulbs
#bulbs[1].alert!

bulbs.each do |bulb|
    bulb.on = false
    bulb.save # Apply all the changes you've made
end


