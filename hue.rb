#!/usr/bin/env ruby

require 'huey'
require "pp"

Huey.configure do |config|
  # For discovering the Hue hub, usually you won't have to change this
  config.ssdp_ip = '239.255.255.250' 

  # Also for discovering the Hue hub
  config.ssdp_port = 1900            

  # If you get constant errors about not being able to find the Hue hub and you're sure it's connected, increase this
  config.ssdp_ttl = 1

  # Change this if you don't like the included uuid
  config.uuid = '001788fffe0a5dfc'
end


bulbs = Huey::Bulb.all # Returns an array of your bulbs
bulbs = []
bulbs << Huey::Bulb.find(2)
bulbs << Huey::Bulb.find(3)
#pp bulbs
#bulbs[1].alert!

bulbs.each do |bulb|
    bulb.bri = 100 # Let's dim the bulb a little bit
    bulb.ct = 500 # And make it a little more orange

    bulb.save # Apply all the changes you've made

    bulb.update(bri: 100, ct: 500) # Set and save in one step

    bulb.rgb = '#00F1F5' # Everyone loves aqua

    bulb.commit # Alias for save
end


